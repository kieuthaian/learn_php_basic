<?php
$messageArr = [1 => 'Thêm mới thành công', 2 => 'Sửa thành công', 3 => 'Xóa thành công'];
function redirect($path)
{
    header("Location: " . $path);
    exit;
}

function get_message($code)
{
    global $messageArr;
    if (array_key_exists($code, $messageArr)) {
        return $messageArr[$code];
    }
    return false;
}

function makeChecked($id) {
    if (!empty($_GET['hobby'])) {
        $hobby = $_GET['hobby'];
    } else {
        $hobby = [];
    }

    if (!empty($_POST['hobby']) && in_array($id, $_POST['hobby'])) {
        $checked = 'checked';
    } elseif (!empty($hobby) && in_array($id, $hobby)) {
        $checked = 'checked';
    } else {
        $checked = false;
    }
    return $checked;
}