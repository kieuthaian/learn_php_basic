<?php
// Hàm upload files (1 file, nhiều files)
function uploadFile($config, $fieldName, $file = [])
{
    if (!empty(is_array($file))) {
        $_FILES[$fieldName] = $file;
    }
//    echo '<pre>';
//    print_r($_FILES);
//    echo '</pre>';


    if (!empty($_FILES[$fieldName])) {
        $errors = [];
        // Lấy tên file
        $fileName = $_FILES[$fieldName]['name'];

        // Đổi tên file
        $fileNameArr = explode('.', trim($fileName));
        $fileExt = end($fileNameArr); // đuôi file

        $fileBefore = sha1(uniqid()); // đổi tên đầu file
        $fileName = $fileBefore . '.' . $fileExt; // gán tên file mới

        if (!empty($config['file_name'])) {
            $fileName = $config['file_name'];
        }

        // Kiểm tra xem file đã được chọn chưa
        if ($_FILES['file_upload']['error'] == 4) {
            $errors[] = 'choose_file';
        } else {
            // Kiểm tra định dạng được phép
            if (!empty($config['allowed'])) {
                $allowArr = explode(',', $config['allowed']);
                $allowArr = array_filter($allowArr);
                foreach ($allowArr as $key => $allow) {
                    $allowArr[$key] = trim($allow);
                }
                if (!empty($allowArr) && !in_array($fileExt, $allowArr)) {
                    $errors[] = 'allow_ext';
                }
            }

            // Kiểm tra size
            if (!empty($_FILES[$fieldName]['size'])) {
                $size = $_FILES[$fieldName]['size'];
                if (!empty($config['max_size']) && $size > $config['max_size']) {
                    $errors[] = 'max_size';
                }
            } else {
                $errors[] = 'file_error';
            }

        }

        // Kiểm tra mảng errors để thực hiện upload
        if (empty($errors)) {
            $upload = move_uploaded_file($_FILES['file_upload']['tmp_name'], $config['upload_dir'] . '/' . $fileName);
            if ($upload) {
                return ['status' => 'success', 'fileOr' => $_FILES[$fieldName]['name'], 'fileName' => $fieldName, 'size' => $_FILES[$fieldName]['size'], 'path' => $config['upload_dir'] . '/' . $fileName];
            } else {
                return false;
            }
        }
    }

    if (!empty($errors)) {
        $errors['status'] = 'error';
    }
    return $errors;
}

function uploadFileMultip($fieldName)
{
    $fileArr = [];
    if (!empty($_FILES[$fieldName])) {
        foreach ($_FILES[$fieldName]['name'] as $key => $fileName) {
            $fileArr[] = ['name' => $fileName, 'tmp_name' => $_FILES[$fieldName]['tmp_name'][$key], 'error' => $_FILES[$fieldName]['error'][$key], 'type' => $_FILES[$fieldName]['type'][$key], 'size' => $_FILES[$fieldName]['size'][$key]];
        }
    }

    return $fileArr;
}