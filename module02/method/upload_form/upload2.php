<?php
// Kiểm tra phương thức post
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Lấy thông tin file
    $error = []; // Lưu trữ lỗi khi upload

    $allowedArr = ['mp4', 'jpg', 'png', 'gif', 'mov', 'jpg', 'jpeg']; // tạo mảng chứa các đuôi chấp nhận

    $maxFileSize = 5242880;

    $pathUpload = './uploads';

    if (!empty($_FILES['file_upload'])) {
        // Lấy tên file
        $fileName = $_FILES['file_upload']['name'];
        // Đổi tên file
        $fileNameArr = explode('.', trim($fileName));
        $fileExt = end($fileNameArr); // đuôi file
        $fileBefore = sha1(uniqid()); // đổi tên đầu file

//        $fileBefore = str_replace('.'.$fileExt, '', $fileName);
//        $fileBefore = $fileBefore.'_'.uniqid();
        $fileName = $fileBefore . '.' . $fileExt; // gán tên file mới

        // Kiểm tra xem file đã được chọn chưa
        if ($_FILES['file_upload']['error'] == 4) {
            $error['choose_file'] = 'Vui lòng chọn file';
        } else {
            // Kiểm tra định dạng được phép
            if (!in_array($fileExt, $allowedArr)) {
                $error['allow_ext'] = 'Định dạng không được phép, chỉ chấp nhận' . implode(', ', $allowedArr);
            }

            // Kiểm tra size
            if (!empty($_FILES['file_upload']['size'])) {
                $size = $_FILES['file_upload']['size'];
                if ($size > $maxFileSize) {
                    $error['max_size'] = 'Dung lượng vượt quá cho phép, chỉ được upload <= ' . number_format($maxFileSize) . ' byte';
                }
            } else {
                $error['file_error'] = 'File bị lỗi, vui lòng kiểm tra';
            }
        }

        // Kiểm tra mảng error để thực hiện upload
        if (empty($error)) {
            $upload = move_uploaded_file($_FILES['file_upload']['tmp_name'], $pathUpload . '/' . $fileName);
            if ($upload) {
                echo 'Upload thành công';
            } else {
                echo 'Upload ko thành công';
            }
        } else {
            foreach ($error as $item) {
                echo $item . '<br/>';
            }
        }
    }
}