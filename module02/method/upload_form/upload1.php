<?php
////Lấy thông tin của file
//echo '<pre>';
//print_r($_FILES);
//echo '</pre>';

// Kiểm tra phương thức post
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // Lấy thông tin file
    echo '<pre>';
    print_r($_FILES);
    echo '</pre>';
    if (!empty($_FILES['file_upload'])) {
        // Lấy tên file
        $fileName = $_FILES['file_upload']['name'];
//        echo $fileName.'<br/>';

        // Đổi tên file
        $fileNameArr = explode('.', trim($fileName));
//        echo '<pre>';
//        print_r($fileNameArr);
//        echo '</pre>';

        $fileExt = end($fileNameArr); // đuôi file
//        echo $fileExt;
        $fileBefore = sha1(uniqid()); // đổi tên đầu file
//        echo $fileBefore;


//        $fileBefore = str_replace('.'.$fileExt, '', $fileName);
//        $fileBefore = $fileBefore.'_'.uniqid();
//        echo $fileBefore.'<br/>';
        $fileName = $fileBefore . '.' . $fileExt; // gán tên file mới
//        echo $fileName;

        $allowedArr = ['mp4', 'jpg', 'png', 'gif', 'mov']; // tạo mảng chứa các đuôi chấp nhận
        if (in_array($fileExt, $allowedArr)) {
            // Kiểm tra kiểu file hợp lệ
            $size = $_FILES['file_upload']['size'];
            if ($size <= 5242880) {
                $upload = move_uploaded_file($_FILES['file_upload']['tmp_name'],'./uploads/'.$fileName);
                if ($upload) {
                    echo 'Upload thành công';
                } else {
                    echo 'Upload ko thành công';
                }
            } else {
                echo 'Dung lượng vượt quá cho phép';
            }
        } else {
            echo 'Kiểu file ko hợp lệ';
        }
    }
}