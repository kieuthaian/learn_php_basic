<?php
require_once './functions.php'; // import file function
require_once './importFunctions.php';
/*
 *  Cú pháp định nghĩa hàm
 *
 *  function ten_ham(danh_sach_tham_so) {
 *          // Nội dung hàm
 * }
 * - ten_ham: Do chúng ta tự đặt (động từ)
 * - danh_sach_tham_so: Danh sách các biến cách nhau bởi dấu phẩy
 * - Bên trong cặp ngoặc {} là nội dung hàm
 *
 * Cú pháp gọi hàm
 * ten_ham (danh_sach_tham_tri)
 */


// Gọi hàm
if (function_exists('makeTotal')) {
    makeTotal(6, 7);
}

echo '<br/>';
$numberA = 10;
$numberB = 20;

if (function_exists('makeTotal')) {
    makeTotal($numberA, $numberB);
}

if (function_exists('getMessage')) {
    // Gọi hàm message
    getMessage();
}
echo '<br/>';

// Gọi hàm getNumber
if (function_exists('getNumber')) {
    getNumber(12, 'Tớ là tớ vui lắm');
}
echo '<br/>';

// Gọi hàm buildMenu
if (function_exists('buildMenu')) {
    buildMenu(true, 'Đây là menu');
}







