<?php
/*
 * - Hàm return, hàm không return
 * - Biến cục bộ, biến toàn cục, biến tĩnh
 * - Tham biến, tham trị, tham số, tham chiếu
 */
// 1. Hàm return và hàm ko return
// - Hàm ko có từ khóa return => Hàm ko return
// - Hàm có từ khóa return khi kết thúc hàm => Hàm return (Hàm có giá trị trả về)
// Nếu từ khóa return được gọi ra => Các câu lệnh phía dưới ko chạy

// 2. Biến cục bộ, biến toàn cục
/*
 * - Biến cục bộ là biến được sử dụng trong phạm vi hàm
 * - Biến cục bô ko được sử dụng bên ngoài hàm hoặc trong hàm khác
 *
 * - Biến toàn cục là biến được sử dụng ở bất kỳ đâu
 * - Biến toàn cục được khai báo ở ngoài hàm
 * - Muốn sử dụng biến toàn cục bên trong hàm => Phải khai báo bằng từ khóa global
 *
 * - Biến tĩnh: Ko thay đổi giá trị khi gọi hàm
 * - Tham biến: Làm thay đổi giá trị truyền vào
 * - Tham trị: Ko làm thay đổi giá trị truyền vào
 * - Tham số: Được sử dụng khi định nghĩa hàm
 * - Tham chiếu: $b được khai báo tham chiếu đến $a => $b thay đổi => $a thay đổi theo
 */

$data = 1;
$msg = 1;
if (!function_exists('makeTotal')) {
    function makeTotal($a, $b)
    {
        global $data;
        $total = $a + $b + $data;
//        echo $total;
        return $total;
    }
}
//$total = makeTotal(1, 2);
//$total++;
//echo $total;
//echo '<br/>';

function getMessage($smsg = null)
{
    if ($smsg == null) {
        return;
    }
    return $smsg;
}

//echo getMessage('Unicode');
//echo '<br/>';
//echo $data;

function setMsg($data)
{
    global $msg;
    echo $msg = $data;
}

//
//setMsg('Ronaldo');
//echo $msg;

function handleCount()
{
    static $number = 0;
    $number++;
    return $number;
}

echo handleCount();
echo '<br/>';
echo handleCount();

function setMessage($msg)
{
    $msg .= '1';
    return $msg;
}

//echo '<br/>';
//$message = 'Unicode';
//echo '<br/>';
//echo 'Giá trị ban đầu'. $message;
//echo '<br/>';
//echo setMessage($message);
//echo '<br/>';
//echo $message;
echo '<br/>';
$a = 1;
$b = &$a; // $b =1; //$b được khai báo tham chiếu đến a
$b = 10;
$b = 10;
echo $a . '<br/>';
echo $b . '<br/>';

function &hello() {
    static  $hello = 'Unicode';
    return $hello;
}
$h = hello();
$h = 'Học PHP';
echo $h.'<br/>';
echo hello();
echo '<br/>';

/*
 *  hello() => Chứa 1 object
 * $object = &hello;
 * $object -> method()
 */