<?php
session_start(); // khởi tạo session ở đầu
$_SESSION['username'] = 'Thai An';
$_SESSION['fullname'] = 'An Peter';

// update session
$_SESSION['username'] = 'Kieu Thai An';

// Xóa 1 session chỉ đính
//unset($_SESSION['fullname']);

// Xóa tất cả session
//session_destroy();

echo '<pre>';
print_r($_SESSION);
echo '</pre>';
