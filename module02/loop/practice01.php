<?php
/**
 *  1. Hiển thị số chẵn, số lẻ trong dãy số từ 1 đến 100 : 1 2 3 4 5 ... 100
 */
$startIndex = 1;
$endIndex = 100;
$resultEven = null;
$resultOdd = null;



for ($index = $startIndex ; $index <= $endIndex ; $index++) {
//    echo $i;
    if ($index % 2 == 0) {
        $resultEven .= ' '.$index;
    } else  {
        $resultOdd .= ' '.$index;
    }

}
echo 'Số chẵn là: '.$resultEven;
echo "<br/>";
echo 'Số lẻ là: '.$resultOdd;
?>

<?php
/**
 * Tính giai thừa của số !n = 1*2*3*4
 */
echo '<br/>';
$n = 3;
if($n > 0) {
    $result = 1;
    for ($i = 1; $i<= $n; $i++) {
        $result *= $i;
    }
    echo $n.' ! = '.$result.'<br/>';
} else {
    echo $n.' không hợp lệ';
}



?>
