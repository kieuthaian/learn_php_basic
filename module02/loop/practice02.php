<?php
/**
 * 3. Kiểm tra n có phải số nguyên tố không
 */
$n = 5;
if ($n > 1) {
    $check = true;
    for ($index = 2; $index < $n; $index ++ ) {
        if($n % $index == 0 ) {
            $check = false;
        }
    }
    if ($check) {
        echo $n.' là số nguyên tố';
    } else {
        echo $n.' không phải là số nguyên tố';
    }
} else {
    echo $n.' không phải là số nguyên tố';
}

/**
 * 4. In bảng cửu chương
 */
?>
<table border="1px" cellspacing="0" cellpadding="0">
        <?php
            for ($i=1;$i<=10;$i++){
                if($i==1 || $j == 6) {
                    echo '<tr>';
                }
                echo '<td>';
                for($j=1;$j<=10;$j++) {
                    echo $i.' x '.$j.' = '.$i*$j.'<br/>';
                }
                echo '</td>';
                if($i == 5 || $j == 10) {
                    echo '</tr>';
                }
            }
        ?>
</table>
