<?php
// Xác định ố lần lặp
$count = 20;

// Xác định giá trị ban đầu
$start = 0;

// Vòng lặp for
//for ($i = $start; $i <= $count; $i++) {
//    echo 'Đây là vòng lặp '.$i. '<br/>';
//}

// Ví dụ Tính tổng: S = 1 +2+3+4+5+6+7+..+n

$n = 10;
$sum = 0;
for ($i=1; $i<=$n; $i++) {
     echo $sum += $i ;
     echo '<br/>';
}
echo 'Tổng S = '.$sum;