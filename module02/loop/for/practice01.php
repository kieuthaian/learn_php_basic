<?php
/**
 * 1. Hiển thị số chẵn, số lẻ trong dãy số 1 2 4 5 ... 100
 */

$startIndex = 1;
$endIndex = 100;

$resultEven = null;
$resultOdd = null;

$evenCount = 0;
$oddCount = 0;
for ($index = $startIndex; $index <= $endIndex; $index++) {
//    echo $index.'<br/>';
    // Kiểm tra số chẵn
    if ($index % 2 === 0) {
//        echo $index.' là số chẵn';
        $resultEven .= $index. ' ';
        $evenCount++;
    } else {
//        echo $index.' là số lẻ';
        $resultOdd .= $index. ' ';
        $oddCount++;
    }
}

echo 'Tìm thấy '.$evenCount.' Số chẵn: '.$resultEven.'<br/>';
echo 'Tìm thấy '.$oddCount.' Số lẻ: '.$resultOdd.'<br/>';


/**
 * Tính giai thừa của một số và hiển thị kết quả
 * input: nhập vào so n
 * output: hien thi ket qua N!
 *
 * formula: N! = 1*2*3*4*...*n (n>0)
 */

$numStart = 1;
$numEnd = 3;

$factorial = 1;

for ($num = $numStart; $num <= $numEnd; $num++) {
    $factorial *= $num;
}

echo '<br/>';
echo 'Giai thừa của '.$num.' có kết quả là: '.$factorial;
echo '<br/>';