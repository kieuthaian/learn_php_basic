<?php
/** Các hàm xử lý chuỗi */
//$str = 'Trung tâm đào tạo lập trình Unicode';

// Xử lý chuỗi
// 1. Thêm ký tự escape vào phía trước các ký tự mong muốn
//addcslashes($str, $listChar)

//$str = addcslashes($str, 'Ul');
//echo $str;
//echo '<br/>';

// 2. Thêm ký tự escape vào phía trước dấu ", ', \

//$str1 = 'Trung tâm đào tạo lập trình "Unicode"';
$str1 = "Trung tâm đào tạo lập trình 'Unicode'";
$str1 = addslashes($str1);
echo $str1;
echo '<br/>';

// 3. Loại bỏ tất cả ký tu có trong chuỗi stripslashes
$str = 'Trung tâm đào tạo lập trình "Unicode"';
$str = addslashes($str);
echo $str;
echo '<br/>';
$str = stripslashes($str);
echo $str;
echo '<br/>';

// 4. Hàm chuyển chuỗi thành mảng
$str = 'Trung tâm Unicode';
$arr = explode(' ', $str);
print_r($arr);
echo '<br/>';

// 5. Chuyển mảng thành chuỗi
$str = implode(' - ', $arr);
echo $str;
echo '<br/>';

// 6. Hàm trả về độ dài của chuỗi
$str = 'Trung tâm Unicode';
$length = strlen($str);
echo $length;
echo '<br/>';

// 7 . hàm trả về số chữ trong chuỗi
$str = 'Learning PHP at Unicode';
$wordCount = str_word_count($str);
echo 'Số chũ là '. $wordCount;
echo '<br/>';

// 8. Lặp chuỗi theo số lần xác định
$str = 'Unicode';
$strRepeat = str_repeat($str, 2);
echo $strRepeat;
echo '<br/>';

// 9. Tìm kiếm và thay thế
//$str = 'Trung tâm đào tạo lập trình Unicode';
$str = 'C:\xampp\htdocs\online_php';
//$str = str_replace('Unicode', 'CodLuck', $str);
$str = str_replace('\\','/', $str);
echo $str;
echo '<br/>';

// 10. Hàm mã hóa 1 chiều (md5) (32 ky tu)
$str = '123456789';
$str = md5($str);
echo $str.'<br/>';
echo strlen($str).'<br/>';

// 11. Hàm mã hóa 1 chiều trả về 40 ky tự (sha1)
$str = '123456789';
$str = sha1($str);
echo $str.'<br/>';
echo strlen($str).'<br/>';

// 12. Chuyển chuỗi HTML thành dạng thực thể
$str = '<p>Đào tạo PHP</p>';
$str = htmlentities($str);
echo $str;
echo '<br/>';

// 13. Chuyển từ dạng thực thể sang chuỗi HTML
$str = html_entity_decode($str);
echo $str;
echo '<br/>';

// 14. Loại bỏ thẻ html
$str = '<p>Trung tâm đào tạo <a href="">Lập trình <strong>Unicode</strong></a></p>';
$str = strip_tags($str, '<a><strong>');
echo $str;
echo '<br/>';

// 15. Lấy chuỗi con từ chuỗi cha
$str = 'Học lập trình tại Unicode';
$subStr = substr($str, 0, 10 );
echo $subStr;
echo '<br/>';

// 16. Tách chuỗi từ ký tự cho trước cho đến hết chuỗi
$str = 'kieuthaian0212@gmail.com';
$subStr = strstr($str, '@');
echo $subStr;
echo '<br/>';

// 17. Tìm chuỗi và trả về số thứ tự
$str = 'Trung tâm dao tao tai Unicode';
$position = strpos($str, 'google');
var_dump($position);
echo '<br/>';

// 18. Hàm cắt bỏ và thay thế chuỗi
/**
 *  chuỗi cần thay
 *  thay
 *  Chèn 0
 *  Đè 1 ký tự
 */
$str = 'Trung tâm dao tao tai Unicode';
$str = substr_replace($str, '@', '0', '1');
echo $str;
echo '<br/>';

// 19. Chuyển các ký tự sang dạng viết thường
$str = 'Trung tam Đào tao Unicode';
//$str = strtolower($str);
$str = mb_strtolower($str, 'UTF-8'); // hàm ho tro tieng viet co dau
echo $str;
echo '<br/>';

// 20. CHuyển các ky tu thành viết hoa
$str = 'Trung tam dao tao Unicode';
//$str =strtoupper($str);
$str = mb_strtoupper($str, 'UTF-8');
echo $str;
echo '<br/>';

// 21. CHuyển chu cai dau tien thanh viet hoa
$str = 'trung tam unicode';
$str = ucfirst($str);
echo $str;
echo '<br/>';

// 22. CHuyển chư cai dau tien thanh chu thuong
$str = 'Trung tam unicode';
$str = lcfirst($str);
echo $str;
echo '<br/>';

// 23. CHuyển ký tự đầu tiên thuộc chữ trong chuỗi
$str = 'trung tâm unicode';
$str = ucwords($str);
echo $str;
echo '<br/>';

// 24. XÓa ký tự đầu cuối
$str = ' Trung tâm Unicode ';
$str = trim($str);
echo $str;
echo '<br/>';

// 25. XÓa ký tự bên trái
$str = ' Trung tâm unicode';
$str = ltrim($str);
echo $str;
echo '<br/>';

// 26. XÓa ky tu ben phai
$str = 'Trung tam Unicode ';
$str = rtrim($str);
echo $str;
echo '<br/>';

// 27. Tách chuỗi lớn thành từng chuỗi nhỏ
$str = '121212';
$str = chunk_split($str, 2, ':');
$str = ltrim($str);
echo $str;
echo '<br/>';

// 28. Chuyển ký tự xuống dòng (\n) thành thẻ <br/>
$str = 'Trung tam dao tao '."\n".' Unicode';
$str = nl2br($str);
echo $str;
echo '<br/>';

// 29. Chuyển JSON thành mảng hoặc Object
$strJson = '{"item1":"Value1", "item2":"Value2"}';
echo $strJson;
echo '<br/>';
$jsonArr = json_decode($strJson, true);
print_r($jsonArr);
echo '<br/>';

// 30. CHuyển từ mảng, object thành Json
$strJson = json_encode($jsonArr);
echo $strJson;