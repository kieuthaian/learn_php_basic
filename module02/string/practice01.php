<?php
/**
 *  Bài 1: Viết chương trình lấy username của 1 email
 *  input: kieuthaian0212@gmail.com
 *  output: kieuthaian0212
 */

// Input
$emailInput = 'kieuthaian0212@gmail.com';

// Xử lý
//$endEmail = strstr($emailInput, '@');
//$userName = str_replace($endEmail, '', $emailInput);
$userName = strstr($emailInput, '@', true);

// Output
echo $userName;

/*
 * Bài 2: Viết chương trình lấy 5 ký tự cuối chuỗi
 */

$strInput = 'Học lập trình tại Unicode';
$endStr = mb_substr($strInput, -5, null, 'UTF-8');
echo '5 ký tự cuối chuỗi là ' . $endStr . '<br/>';

/*
 * Viết chương trình xóa chữ đầu tiên trong chuỗi
 * Input: Kiều Thái An
 * Output: Thái An
 */
$strInput = 'Kiều Thái An';
$positionSpace = mb_strpos($strInput, ' ', null, 'UTF-8');
//$firstWord = mb_substr($strInput, 0, $positionSpace+1, 'UTF-8');
//$strInput = str_replace($firstWord, '', $strInput);
$leftLength = mb_strlen($strInput, 'UTF-8') - ($positionSpace + 1);
$endWord = mb_substr($strInput, $positionSpace + 1, $leftLength, 'UTF-8');


// Output
echo $endWord;
echo '<br/>';
/*
 * Bài 4: Viết chương trình đảo ngược chữ đầu và chữ cuối trong chuỗi
 */
$strInput = 'Học lập trình PHP tại Unicode';
// 1. Tìm chữ đầu tiên
$positionSpaceFirst = mb_strpos($strInput, ' ', null, 'UTF-8');
$firstWord = mb_substr($strInput, 0, $positionSpaceFirst, 'UTF-8');
// 2. Tìm chữ cuối cùng
$positionSpaceEnd = mb_strripos($strInput, ' ', null, 'UTF-8');
$leftLength = mb_strlen($strInput, 'UTF-8') - $positionSpaceEnd;
$endWord = mb_substr($strInput, $positionSpaceEnd + 1, $leftLength, 'UTF-8');

// 3. Chèn và thay thế
// Lấy nội dung giữa chuỗi
$middleWord = mb_substr($strInput, $positionSpaceFirst, $positionSpaceEnd - $positionSpaceFirst+1, 'UTF-8');

// Chèn và đổi chỗ
$strInput = $endWord.$middleWord.$firstWord;



// Output
echo $strInput.'<br/>';