<?php
/**
 * Bài 5: Nhập vào tên 1 người, viết chương trình in ra
 * - Họ và tên lót
 * - Tên
 */
$fullName = 'Kiều Thái An';

// Handle
$positionSpaceLast = mb_strripos($fullName, ' ', null, 'UTF-8');
$leftLength = mb_strlen($fullName, 'UTF-8');
$firstName = mb_substr($fullName, $positionSpaceLast + 1, $leftLength, 'UTF-8');
var_dump($firstName);

$lastName = mb_substr($fullName, 0, $positionSpaceLast, 'UTF-8');


// Output
echo 'FirstName: '.$firstName.'<br/>';
echo 'LastName: '. $lastName.'<br/>';


/*
 * Bài 6: In ra trong chuỗi 50 chữ đầu tiên trong chuỗi
 */
// Input
$content = 'Trang chủ Liên đoàn Bóng đá Bồ Đào Nha (PFF) đưa tin Pereira bị gãy 3 xương sườn bên phải trong buổi tập mới đây của tuyển Bồ Đào Nha. PFF không tiết lộ thời điểm cụ thể cầu thủ 31 tuổi này quay trở lại thi đấu.
Trước đó, Pereira đá cặp trung vệ với Ruben Dias trong thắng lợi 3-2 của Bồ Đào Nha trước Ghana hôm 24/11 (giờ Hà Nội). Ở trận đó, Pereira lãnh thẻ vàng sau pha phạm lỗi với tiền đạo Inaki Williams của đối thủ.
Trong 12 trận gần nhất của Bồ Đào Nha, Pereira đều có tên trong đội hình xuất phát của HLV Fernando Santos. Anh có sở trường đá tiền vệ phòng ngự nhưng cũng có thể chơi trung vệ nếu cần. Pereira có 64 lần ra sân cho ĐTQG trong sự nghiệp.';

$limitWord = 50;
// Handle
$contentLength = mb_strlen($content, 'UTF-8');
echo $contentLength.'<br/>';
$description = null;
$count = 0;
for ($i = 0; $i < $contentLength; $i++) {
    $description .= mb_substr($content, $i, 1, 'UTF-8');
    if(mb_substr($content, $i, 1, 'UTF-8') == ' ') {
        $count++;
        if ($count >= $limitWord) {
            break;
        }
    }
}
//echo $count;


// Output
echo $description;

echo '<br/>';

/**
 * Bài 7: Viết chương trình kiểm tra độ mạnh của mật khẩu
 *
 * - Có độ dài tối thiểu là 6
 * - Chứa ít nhất 1 chữ số (1234567890)
 * - Chứa ít nhất 1 kí tự chữ cái in thường (abc...z)
 * - Chứa ít nhất 1 kí tự đặc biệt: !@#$%^&*()-+
 *
 */

$password = 'kieuthaian@1999';
$number = '123456789';
$symbol = '!@#$%^&*()-+';

$checkLength = false;
$checkNumber = false;
$checkLower = false;
$checkUpper = false;
$checkSymbol = false;

if (mb_strlen($password, 'UTF-8') >= 6) {
    $checkLength = true;
}

for ($i = 0; $i < mb_strlen($password, 'UTF-8'); $i++) {
    $char = mb_substr($password, $i, 1, 'UTF-8');
    if (mb_strpos($number, $char, null, 'UTF-8') != false) {
        $checkNumber = true;
//        break;
    }

    if($char >= 'a' && $char <= 'z') {
        $checkLower = true;
//        break;
    }

    if($char >= 'A' && $char <= 'Z') {
        $checkUpper = true;
//        break;
    }

    if (mb_strpos($symbol, $char, null, 'UTF-8') != false) {
        $checkSymbol = true;
//        break;
    }
}


if ($checkLength && $checkNumber && $checkLower && $checkUpper && $checkSymbol) {
    echo 'Mật khẩu mạnh';
} else {
    echo 'Mật khẩu yếu';
}