<?php
// string la gi
/**
 * Chuỗi là danh sách các ký tự nằm trong dấu nháy đơn hoặc dấu nháy kép
 *
 */

$exStr = 'Học lập trình PHP';
var_dump($exStr);
echo '<br/>';

$exStr = "Học lập trình PHP";
var_dump($exStr);
echo '<br/>';
$exHtml = '<p><a href="http://localhost">Unicode</a></p>';
echo $exHtml;
echo '<br/>';

const _EX_HTML = '<h3>Học lập trình PHP</h3>';
echo _EX_HTML;
echo '<br/>';

$exStr = "Trung tâm đào tạo \"Unicode\" Hà Nội";
echo $exStr;
echo '<br/>';

/**
 * Nguyên tắc làm việc với chuỗi
 */

$exStr = 'Trung tâm đào tạo "Unicode" Hà Nội';
echo $exStr;
echo '<br/>';

$exHtml = "<p><a href=\"http://localhost\">Unicode</a></p>";
echo $exHtml;
echo '<br/>';

/** Nối chuỗi */
/**
 *  Để nối chuỗi trong PHP, chúng ta sẽ sử dụng dấu chấm
 *
 */
$selectHtml = '<select name="category">';
for ($i = 2000; $i <= 2021; $i++) {
    $selectHtml .= '<option value="'.$i.'">Năm '.$i.'</option>';
}
$selectHtml.='</select>';

echo $selectHtml;










