<?php
// 1. Kiểu số nguyên (Int)
// Khai báo
$age = 23;
//var_dump($age);

// ép kiểu
$age1 = 23.5;
//$age1 = (int)$age1;

// Kiểm tra kiểu số nguyên
//$check = is_int($age1);
//$check = is_integer($age1);
//var_dump($check);

// 2. Kiểu boolean (logic)
$check = 0;
$check = (bool)$check;
$checkBool = is_bool($check);
//var_dump($check);

// 3. Kiểu số thực
$fee = 10;
$fee = (float)$fee;
//var_dump($fee);

// 4. Kiểu chuỗi
$str = "Hello AnPeter";
$str1 = 10;
$str1 = (string)$str1;
$checkStr = is_string($str1);
//var_dump($checkStr);

// 5. Kiểu mảng
$car_array = [];
$car_array = (array($car_array));
$checkArr = is_array($car_array);
//var_dump($checkArr);

// 6. Kiểu rỗng (null)
$total = null;
$checkNull = is_null($total);
//var_dump($checkNull);

// 7. Kiểu resource
$curl = curl_init();
$checkType = is_resource($curl);
//var_dump($checkType);

// 8. Kiểu đối tượng (Object)
$dataCustomer = [
    'AN PETER'
];
//$dataCustomer = (object)$dataCustomer;
$checkObject = is_object($dataCustomer);
//var_dump($checkObject);

$customerObject = new StdClass();
$customerObject->age = 23;
//var_dump($customerObject);


/**
 * Phân biệt empty và null
 * - Null ko liên quan đến kiểu tính toán
 * - Trống liên quan đến kiểu chuỗi
 * => giữ chố cho biến nào đó thì sử dụng null
 *
 */

$message1 = null; // Rỗng
$message2 = ''; // Trống
var_dump($message1);
echo "<br />";
var_dump($message2);




