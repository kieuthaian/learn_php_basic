<?php
// Toán tử +=
$a = 10;
$a += 10;
echo $a;
echo '<br/>';

// Toán tử -=
$a = 10;
$a -= 5;
echo $a;
echo '<br/>';

// Toán tử *=
$a =10;
$a  *= 10;
echo $a;
echo '<br/>';

// Toán tử /=
$a =10;
$a /= 10;
echo $a;
echo '<br/>';

// Toán tử %=
$a = 10;
$a %= 6;
echo $a;
echo '<br/>';

// Toán tử so sánh
$a = 10;
$b = 11;
$check = $a > $b;
var_dump($check);
echo '<br/>';

// Toán tử == (chỉ so sánh giá trị)
$a = 10;
$b = '10'; // Chuỗi số
$check = $a == $b;
var_dump($check);
echo '<br/>';

// Toán tử === (so sánh giá trị và kiểu dữ liệu)
$a = 10;
$b = '10'; // Chuỗi số
$check = ($a === $b);
var_dump($check);
echo '<br/>';

// Toán tử khác
$a = 10;
$b = 10.0;
$check = $a != $b;
$check1 = $a !== $b;
var_dump($check);
var_dump($check1);
echo "<br/>";

// Toán tử quan hệ
// Toán tử và (&&)
/**
 * - Biểu thức đúng khi tất cả đều đúng
 * - 1 biểu thức con sai thì tất cả đều sai
 */

$a = 10;
$b = 15;
$c = 20;
//$check = $a >= 10 && $b >=16;
$check = $a >= 10 && ($b >= 15 && $c >= 25);
var_dump($check);
echo '<br/>';


// Toán tử hoặc (||)
/**
 * - Cả bieu thức đúng khi 1 trong các biểu thức con đúng
 * - Cả biểu thức sai khi tất ca biểu thức con sai
 */
$a = 10;
$b = 15;
$check = $a >= 10 || $b <= 0;
var_dump($check);
echo '<br/>';

// Toán tử phủ định (!)
$a = false;
$check = !$a;
var_dump($check);
echo "<br/>";

// ĐỘ ưu tiên
// NOT => AND => OR