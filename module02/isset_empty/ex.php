<?php
// Hàm isset()
/*
 * - Kiểm tra biến có tồn tại hay ko
 * - Ko kiểm tra về dữ liệu của biến
 * - Ko kiểm tra được trường hợp null
 *
 */
//$number = 'Unicode';
//$check = isset($number);
//var_dump($check);
$number = 'Unicode';
if (isset($number) && $number) {
    echo $number;
    var_dump($number);
} else {
    echo 'Ko có number';
}

// Hàm empty()
/*
 * - Chỉ trả về kiểu dữ liệu boolean
 * - Trả về true nếu :
 * + Ko tồn tại => !isset($variavle)
 * Hoặc
 * + Rỗng, =0, trống, null, arr rỗng, object rỗng, false
 *
 * Ứng dụng:
 * Kiểm tra biến tồn tại và có dữ liệu
 */
echo '<br/>';
//$object = new stdClass();
//$str = (array)$object;
//$check = empty($str);
//var_dump($check);

//$str = 'Ronaldo';
// Kiểm tra biến $number tồn tại và có dữ liệu
if (!empty($str)) {
    echo $str;
} else {
    echo 'Ko hợp lệ';
}