<?php
/**
 * Thứ tự import
 *  - Header
 *  - Content
 *  - Footer
 *
 *  Cú pháp include
 *  include 'path-to-php-file';
 *  include('path-to-php-file);
 */

//$path_dir = __DIR__.'\includes';
$path_dir = './includes';
// Import header.php
require_once $path_dir.'/header.php';
//include_once $path_dir.'/header.php';

// Import content.php
require_once $path_dir.'/content.php';

// Import footer.php
require_once $path_dir.'/footer.php';

//echo $path_dir;

/**
 * 1. include, include_once => nếu lỗi => Chương trình phía dưới vẫn chạy
 * 2. require, require_once => nếu lỗi => chương trình phía dưới dừng (fatal error)
 */

?>
