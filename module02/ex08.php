<?php
/**
 * Kiem tra thu trong tuan
 * $number = 1 => chu nhat
 * $number = 2 => thu hai
 * $number = 3 => thu ba
 * $number = 4 => thu tu
 * $number = 5 => thu nam
 * $number = 6 => thu sau
 * $number = 7 => thu bay
 */
$number = 10;
switch ($number) {
    case 1:
        //code case 1
        echo "Hom nay la chu nhat";
        break;

    case 2:
        //code case 2
        echo "Hom nay la thu hai";
        break;

    case 3:
        //code case 3
        echo "Hom nay la thu ba";
        break;

    case 4:
        //code case 4
        echo "Hom nay la thu tu";
        break;

    case 5:
        //code case 5
        echo "Hom nay la thu nam";
        break;

    case 6:
        //code case 6
        echo "Hom nay la thu sau";
        break;

    case 7:
        //code case 7
        echo "Hom nay la thu bay";
        break;

    default:
        //code default
        echo "Khong dung dinh dang ";
        break;
}

echo '<br/>';

/**
 *  Kiem tra so ngay trong thang
 * Input: Nhap thang, nam
 * Output: Dua ra so ngay trong thang
 *
 * Thang 31 ngay: 1, 3, 5, 7, 8, 10, 12
 * Thang 30 ngay: 4, 6, 9, 11
 * Thang 28, 29 ngay: 2 (Nam nhuan = nam chia het cho 4)
 */
//Input

$month = 2;
$year = 2020;

switch ($month) {
    case 2:
        if($year % 4 == 0) {
            echo "Thang " . $month . ' co 29 ngay';
        } else {
            echo "Thang " . $month . ' co 28 ngay';
        }

        break;

    case 4:
    case 6:
    case 9:
    case 11:
        echo "Thang ".$month.' co 30 ngay';
        break;

    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
        echo "Thang ".$month." co 31 ngay";
        break;

    default:
        echo "Khong dung dinh dang thang";
        break;
}
