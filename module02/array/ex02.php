<?php
/*
 * - Vấn đề khi đọc duyệt mảng
 * - Mảng đa chiều
 * - Duyệt (đọc) mảng đa chiều
 */
$customerArr = 'Lief';

//if (!empty($customerArr) && is_array($customerArr)) {
//    foreach ($customerArr as $key => $value) {
//        echo $value . '<br/>';
//    }
//} else {
//    echo 'Mảng ko hợp lệ';
//}
/*
 * Trước khi duyệt mảng:
 * 1 Kiểm tra biến tồn tại
 * 2 Kiểm tra biến là mảng
 * 3. - Kiểm tra mảng có phần tử
 * -> 1 và 3 sử dụng hàm empty
 */
if (is_array($customerArr) && !empty($customerArr)) {
    foreach ($customerArr as $key => $value) {
        echo $value . '<br/>';
    }
} else {
    echo 'Mảng ko hợp lệ';
}

$customerArr = [
    'Thái AN',
    'Hoàng Khang',
    'Quang Long',
    'Tuấn Minh'
];
echo '<pre>';
print_r($customerArr);
echo '</pre>';
for ($i = 0; $i <= count($customerArr); $i++) {
    if (isset($customerArr[$i])) {
        var_dump($customerArr[$i]);
    }
}