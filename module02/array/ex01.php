<?php
/*
 *  Kiểu dữ liệu đơn: string, boolean, null, number
 */

/*
 * Cộng 100 số nguyên với nhau
 * Cách xử lý
 * $number1 = 1;
 * $number2 = 10;
 * $number3 = 100;
 */

$carArr = [
    'Vinfast',
    'Honda',
    'Kia',
    'Toyota',
    null,
    30
];

//echo $carArr;
var_dump($carArr);

echo '<pre>';
print_r($carArr);
echo '</pre>';

$customerArr = [
    'Unicode',
    'name' => 'Thái An',
    10 => 'PHP',
    'position' => 'Fresher Dev',
    'age' => 23,
    'HTML - CSS'
];

echo '<pre>';
print_r($customerArr);
echo '</pre>';

// Thêm phần tử vào mảng
/*
 * Cách 1: Thêm khi khai báo
 * $customerArr = ['Item1','Item2','Item3']
 *
 * Cách 2: Thêm sau khi khai báo
 *
 */

$customerArr = [];

// Thêm phần tử
$customerArr[] = 'Unicode';
$customerArr[] = 'PHP';
$customerArr[] = 'HTML-CSS';
$customerArr[] ='Thái An';

// Sửa giá trị phần tử mảng
//$customerArr['name'] = 'Kiều Thái AN';
//$customerArr['age'] = 23;
//$customerArr[11] = 'Javascript';

// Xóa phần tử của mảng
//unset($customerArr['age']);

echo '<pre>';
print_r($customerArr);
echo '</pre>';

// Đọc mảng tuần tự (index tăng dần đều từ 0)
for ($index = 0; $index < count($customerArr); $index++) {
    echo $customerArr[$index].'<br/>';
}

$customerArr = [
    'Unicode',
    'name' => 'Thái An',
    10 => 'PHP',
    'position' => 'Fresher Dev',
    'age' => 23,
    'HTML - CSS'
];
echo '<br/>';
// 2 ĐỌc mảng bất kỳ
foreach ($customerArr as $item) {
    echo $item.'<br/>';
}