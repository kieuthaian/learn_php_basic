<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Menu Dropdown</title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #333;
        }

        li {
            float: left;
        }

        li a, .dropbtn {
            display: inline-block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }

        li a:hover, .dropdown:hover .dropbtn {
            background-color: red;
        }

        li.dropdown {
            display: inline-block;
        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
        }

        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
            text-align: left;
        }

        .dropdown-content a:hover {
            background-color: #f1f1f1;
        }

        .dropdown:hover .dropdown-content {
            display: block;
        }
    </style>
</head>
<body>
<?php
$menuArr = [['title' => 'Home', 'link' => '#', 'class' => ''], ['title' => 'News', 'link' => '#', 'class' => ''], ['title' => 'Dropdown', 'link' => '#', 'class' => '', 'sub' => [['title' => 'Link 1', 'link' => '#'], ['title' => 'Link 2', 'link' => '#'], ['title' => 'Link 3', 'link' => '#'],]],];
//echo '<pre>';
//print_r($menuArr);
//echo '</pre>';

if (!empty($menuArr)) {
    echo '<ul>';

    // foreach loop
    foreach ($menuArr as $item) {
        $class = !empty($item['class']) ? 'class="' . $item['class'] . '"' : null;

        $class = !empty($item['sub']) ? 'class="dropdown"' : $class ;

        echo '<li><a' . $class . ' href="' . $item['link'] . '">' . $item['title'] . '</a>';

        // Dropdown here
        if (!empty($item['sub'])) {
            $subMenu = $item['sub'];
            echo '<div class="dropdown-content">';
            foreach ($subMenu as $sub) {
                echo '<a href="'.$sub['link'].'">'.$sub['title'].'</a>';
            }
            echo '</div>';
        }


        echo '</li>';
    }

    echo '</ul>';
}
?>
<!--<ul>-->
<!--    <li><a href="#home">Home</a></li>-->
<!--    <li><a href="#news">News</a></li>-->
<!--    <li class="dropdown">-->
<!--        <a href="javascript:void(0)" class="dropbtn">Dropdown</a>-->
<!--        <div class="dropdown-content">-->
<!--            <a href="#">Link 1</a>-->
<!--            <a href="#">Link 2</a>-->
<!--            <a href="#">Link 3</a>-->
<!--        </div>-->
<!--    </li>-->
<!--</ul>-->


</body>
</html>