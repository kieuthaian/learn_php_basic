<?php
/*
* - mảng đa chiều
* - DUyệt (đọc) mảng đa chiều
*/

//$customerArr = [
//    'Thai An',
//    123,
//    false,
//    null
//];
//echo '<pre>';
//print_r($customerArr);
//echo '</pre>';

/*
 * Mỗi phần tử chứa thông tin một khách hàng
 * - Họ và tên
 * - Email
 * - Số điện thoại
 * - Địa chỉ
 */
//$item1 = [
//    'name' => 'Thái An',
//    'email' => 'kieuthaian0212@gmail.com',
//    'phone' => '0335257862',
//    'address' => 'Hà Đông, Hà Nội'
//];
//$item2 = [
//    'name' => 'Thái An',
//    'email' => 'kieuthaian0212@gmail.com',
//    'phone' => '0335257862',
//    'address' => 'Hà Đông, Hà Nội'
//];

// Khai báo mảng đa chiều có sẵn dữ liệu
$customerArr = [
    [
        'name' => 'Thái An',
        'email' => 'kieuthaian0212@gmail.com',
        'phone' => '0335257862',
        'address' => 'Hà Đông, Hà Nội'
    ],
    [
        'name' => 'Thái An',
        'email' => 'kieuthaian0212@gmail.com',
        'phone' => '0335257862',
        'address' => 'Hà Đông, Hà Nội'
    ],
    'status' => 'success',
    30

];

// Khai báo mảng đa chiều ko có dữ liệu ban đầu
$customerArr = [];

// Nhập dữ liệu vào mảng đa chiều
$customerArr[] = [
    'name' => 'Thái An',
    'email' => 'kieuthaian0212@gmail.com',
    'phone' => '0335257862',
    'address' => 'Hà Đông, Hà Nội'
];
$customerArr[] = [
    'name' => 'Thái An',
    'email' => 'kieuthaian0212@gmail.com',
    'phone' => '0335257862',
    'address' => 'Hà Đông, Hà Nội'
];
$customerArr['status'] = 'success';
$customerArr[] = 30;
$customerArr['ronaldo'] = [
    'name' => 'Thái An',
    'email' => 'kieuthaian0212@gmail.com',
    'phone' => '0335257862',
    'address' => 'Hà Đông, Hà Nội'
];


echo '<pre>';
print_r($customerArr);
echo '</pre>';