<?php
$number = 1;
var_dump($number > 0);
$check = $number > 0;
if (!$check) { // if ($check == true)
    echo $number.' là số nguyên âm';
} else {
    echo $number.' là số nguyên dương';
}

echo '<br/>';

$number = 11;
echo '<br/>';
// Cấu trúc if else kết hợp
/**
 * Nếu < 0 => số âm
 * Nếu = 0 => số 0
 * nếu > 0 và <= 5 => số nhỏ
 * nếu > 5 và <= 10 => số trung bình
 * Nếu > 10 và <= 15 => số lớn
 * Nếu > 15 => S cực lớn
 */

if ($number < 0) {
    echo $number.' là số âm';
} elseif ($number == 0) {
    echo $number.' là số 0';
} elseif ($number > 0 && $number <= 5) {
    echo $number.' là số nhỏ';
} elseif ($number > 5 && $number <= 10) {
    echo $number.' là sô trung bình';
} elseif ($number > 10 && $number <= 15) {
    echo $number.' là số lớn';
} else {
    echo $number.' là số cực lớn';
}

// Cấu trúc if else lồng nhau
echo '<br/>';
$number = 10;
if ($number > 0) {
    if ($number >= 5 ) {
        echo "Thỏa mãn điều kiện";
    } else {
        echo 'Ko thỏa mãn điều kiện';
    }
} else {
    echo 'Ko thỏa man dieu kien';
}