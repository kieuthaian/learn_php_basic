<?php
/*
 * Xử lý ngoại lệ là quá trình xử lý các lỗi giúp chương trình
 * vẫn hoạt động bình thường
 * Cú pháp:
 * try {
 *
 * } catch()
 *
 */

echo 'Học lập trình php tai unicode <br/>';
$age = 23;
try {
    // Tất cả code thực thi sẽ viết tại đây
//    unicode();
    if ($age< 30) {
        throw new Exception('Tuổi phải lớn hơn 30');
    }
} catch (Error $exception) {
    echo $exception->getMessage() . '<br/>';
//    echo $exception->getCode(); trả về 0
//    echo $exception->getLine().'<br/>';
    echo 'File: ' . $exception->getFile() . '- Line: ' . $exception->getLine() . '<br/>';
    echo '<br/>';
} catch (Exception $exception) {
    echo $exception->getMessage() . '<br/>';
}

echo 'Chương trình vẫn chạy';