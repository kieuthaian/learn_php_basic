<?php
/*
 * Tổng quan về object (ĐỐi tượng)
 * - Object là một tập hợp các thuộc tính cụ thể nào đó cho 1 đối tượng cụ thể
 * - Object bao gồm
 * + Hằng số (const)
 * + Thuộc tính (Biến)
 * + Phương thức (Hàm)
 * - Để có object, ta cần phải định nghĩa lớp (class)
 */

/*
 * Cú phap khởi tạo object
 *
 * $tenBien = new TenLop(thamso);
 * hoặc
 * $tenBien = new TenLop();
 * hoặc
 * $tenBien = new TenLop;
 *
 */
$dateObject = new DateTime();
//print_r($dateObject);

/*
 * Cách sử dụng:
 * - Gọi hằng số: $tenBienDoiTuong::tenhang;
 * - Gọi thuộc tính: $tenBienDoiTuong->tenthuoctinh (ko có dấu $)
 * - Gọi phương thức: $tenBienDoiTuong->tenPhuongThuc(thamso)
 *
 */

// 1. Gọi hằng sô
echo $dateObject::RSS.'<br/>';
echo $dateObject::COOKIE.'<br/>';

// 2. Gọi phương thức
echo $dateObject->format('d-m-Y H-i-s').'<br/>';

class DemoObject {
    public $education = 'Unicode';
}

// Tạo đối tượng
$demoObject = new DemoObject();
echo $demoObject->education.'<br/>';

// ví dụ stdClass
$demoStd = new stdClass();
var_dump($demoStd);

$demoStd->name = 'Thai An';
var_dump($demoStd).'<br/>';
echo $demoStd->name;