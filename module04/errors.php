<?php
/*
 * PHP Errors
 * - PARSE: Lỗi cú pháp, chương trình sẽ dừng khi gặp lỗi này
 * - FATAL: Lỗi do không biên dịch được, chương trình sẽ dừng khi gặp lỗi
 * - WARNING: Lỗi cảnh báo không nghiêm trọng, chương trình vẫn chạy
 * - NOTICE: Lỗi không nghiêm trọng giống warning
 *
 */

// 1. Lỗi cú pháp (PARSE)
echo 'Hoc lap trình PHP'. '<br/>';
echo 'Unicode'. '<br/>';

// 2. Lỗi biên dịch (FATAL)
//unicode();
echo 'Chương trình chạy tiếp'. '<br/>';

// 3. Lỗi cảnh báo (WARNING)
include_once 'ronaldo.php';
echo 'CHương trình chạy tiếp'. '<br/>';

// 4. Lỗi cảnh báo (NOTICE)
$str1 = 'Unicode';
echo $str1. '<br/>';
echo $str2. '<br/>';

$arr = [];
echo $arr[0];