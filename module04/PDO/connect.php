<?php
const _HOST = 'localhost';
const _USER = 'root';
const _PASS = ''; // xampp => pass = ''
const _DB = 'phponline';
const _DRIVER = 'mysql';

try {
    if (class_exists('PDO')) {
        $dsn = _DRIVER.':dbname='._DB.';host='._HOST;

        $options = [
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', //Set utf8
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION // Đẩy lỗi vào ngoại lệ khi truy vấn
        ];

        $conn = new PDO($dsn, _USER, _PASS, $options);
//        var_dump($conn);
    }

} catch (Exception $exception) {
    echo 'Lỗi kết nối ' . $exception->getMessage().'<br/>';
    die();
}