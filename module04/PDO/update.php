<?php
require_once './connect.php';

// SQL Query
//$sqlUpdate = 'UPDATE users SET email=:email, fullname=:fullname WHERE id=:id';
$sqlUpdate = 'UPDATE users SET email=:email, fullname=:fullname WHERE id=:id';

// Data
$email = 'ronaldo@gmail.com';
$fullname = 'Ronaldo';
$id = 6;

try {
    $statement = $conn->prepare($sqlUpdate);
//    var_dump($statement);

    $statement->bindParam(':email', $email);
    $statement->bindParam(':fullname', $fullname);
    $statement->bindParam(':id', $id);

//    $data = [
//        'id' => 7,
//        'email' => $email,
//        'fullname' => $fullname
//    ];

    $updateStatus = $statement->execute();
//    $updateStatus = $statement->execute($data);
    var_dump($updateStatus);

} catch (Exception $exception) {
    echo $exception->getMessage() . '<br/>';
    echo 'File' . $exception->getFile() . ' - Line: ' . $exception->getLine();
}