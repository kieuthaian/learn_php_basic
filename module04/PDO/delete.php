<?php
require_once 'connect.php';

$id = 7;

$sqlDelete = 'DELETE FROM users WHERE id = ?';

try {
    $statement = $conn->prepare($sqlDelete);

    $data = [$id];

    $deleteStatus = $statement->execute($data);

} catch (Exception $exception) {
    echo $exception->getMessage() . '<br/>';
    echo 'File' . $exception->getFile() . ' - Line: ' . $exception->getLine();
}