<?php
require_once 'connect.php';

// 1. Truy vấn tất cả dữ liệu
$sqlFetchAll = 'SELECT * FROM users';

$id = 5;

try {
    $statement = $conn->prepare($sqlFetchAll);

    $dataParam = [$id];

    $statement->execute($dataParam);

    $data = $statement->fetchAll(PDO::FETCH_ASSOC);

    echo '<pre>';
    print_r($data);
    echo '</pre>';

} catch (Exception $exception) {
    echo $exception->getMessage() . '<br/>';
    echo 'File' . $exception->getFile() . ' - Line: ' . $exception->getLine();
}


// 2. Truy vấn 1 hàng dữ liệu
$sqlFetch= 'SELECT * FROM users WHERE id = ?';

try {
    $statement = $conn->prepare($sqlFetch);

    $statement->execute([5]);

    $data = $statement->fetch(PDO::FETCH_ASSOC);
    echo '<pre>';
    print_r($data);
    echo '</pre>';
} catch (Exception $exception) {
    echo $exception->getMessage() . '<br/>';
    echo 'File' . $exception->getFile() . ' - Line: ' . $exception->getLine();
}


// 3. Lấy số hàng của câu lệnh truy vấn
$sql = 'SELECT * FROM users';

$id = 5;

try {
    $statement = $conn->prepare($sql);

    $dataParam = [$id];

    $statement->execute($dataParam);

//   $data = $statement->fetchAll(PDO::FETCH_ASSOC);
//
//    echo '<pre>';
//    print_r($data);
//    echo '</
    $countUser = $statement->rowCount();

    echo 'Số hàng: ' . $countUser;

} catch (Exception $exception) {
    echo $exception->getMessage() . '<br/>';
    echo 'File' . $exception->getFile() . ' - Line: ' . $exception->getLine();
}